import os
import shutil
import sys

from subprocess import check_output

import aapt
import genupdate


def copy_files(outputdir):

    appdir = "app"
    valuesdir = os.path.join(appdir, "src", "main", "res", "values")
  

    files = [
        "git-info.txt",
        os.path.join(appdir, "build.gradle"),
        os.path.join(valuesdir, "config_strings.xml"),
    ]

    for f in files:
        shutil.copy(f, outputdir)

    dirs = [
        os.path.join(appdir, "build", "outputs"),
    ]

    for d in dirs:
        bn = os.path.basename(d)
        dst = os.path.join(outputdir, bn)
        shutil.copytree(d, dst)

def updatefile(apkpath, outputdir):
    updates = os.path.join(outputdir, "updates")
    with open(updates, "w") as f:
        f.write(genupdate.gen_update_file(apkpath))

usage = "Usage: {0} (full|essentials)"

if __name__ == "__main__":
    try:
        flavor = sys.argv[1]
    except IndexError:
        print(usage)
        sys.exit(1)

    if not flavor in ('full', 'essentials'):
        print(usage)
        sys.exit(1)

    release_dir = "release"

    if not os.path.isdir(release_dir):
        os.mkdir(release_dir)

    src_apk_name = "app-release.apk"
    src_apk_path = os.path.join("app", src_apk_name)

    version_name = aapt.extract_version(src_apk_path)
 
    outputdir = os.path.join(release_dir, version_name)

    os.mkdir(outputdir)

    copy_files(outputdir)
    
    dst_apk_name = "MAP-{}.apk".format(version_name)
    
    dst_apk_path = os.path.join(outputdir, dst_apk_name)

    shutil.copyfile(
        src_apk_path,
        dst_apk_path)

    updatefile(dst_apk_path, outputdir)
    

    
    

