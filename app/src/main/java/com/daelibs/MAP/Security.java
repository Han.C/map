package com.daelibs.MAP;

import android.util.Base64;
import android.util.Log;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class Security {

    private static String key =  "Aal6asiB/Quain0He!eiPahd3g345f1x";

    public static String padRight(String s, int n) {
        while (s.length() < n) { s+="\0"; }
        return s;
    }

    public static String HexString(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (byte b : bytes) { sb.append(String.format("%02X", b));}
        return sb.toString();
    }

    public static String encrypt(String input) {
        try {
            return encode(input, key);
        } catch (Exception ex) {
            Log.e("ENCRYPT", ex.getMessage());
            return "";
        }
    }

    private static String encode(String input, String p) throws Exception
    {
        SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(), "AES");
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
        byte[] encrypted = cipher.doFinal(input.getBytes());
        return Base64.encodeToString(encrypted, Base64.DEFAULT);
    }
}