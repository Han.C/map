package com.daelibs.MAP;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;

import androidx.core.content.ContextCompat;

import com.daelibs.MAP.Data.ConfigResult;
import com.daelibs.MAP.Data.Constants;
import com.daelibs.MAP.Data.DeviceInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class Util {

    public static String getSerial(Context context) {
        SharedPreferences settings = context.getSharedPreferences(Constants.PREFS_NAME, 0);
        String loggerSerial = settings.getString(Constants.PREFS_LOGGER_SERIAL_NUMBER, null);
        return loggerSerial;
    }

    public static void setLoggerSerialNumber(Context context, String serial) {
        SharedPreferences settings = context.getSharedPreferences(Constants.PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(Constants.PREFS_LOGGER_SERIAL_NUMBER, serial);
        editor.commit();
}







    public static ConfigResult getRegistrationConfig(Context context, String url, String loggerSerial) {
        RegistrationClient s = new RegistrationClient(url);

        ConfigResult result = s.getConfig(loggerSerial, Util.createDeviceConfig(context));

        if (result != null) {

            SharedPreferences settings = context.getSharedPreferences(Constants.PREFS_NAME, 0);
            SharedPreferences.Editor editor = settings.edit();
            editor.putString(Constants.PREFS_CURRENT_REGISTRATION_SERVICE_INSTANCE_NAME, result.getServiceInstanceName());
            editor.putString(Constants.PREFS_INFO_URL, result.getInfoUrl());
            editor.commit();

            if (result.getInfoUrl() != null) {
                Util.setDeviceConfigured(context, true);


            }

            return result;
        }
        return null;
    }
    //
    public static DeviceInfo createDeviceConfig(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        int permissionCheck = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE);
//        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
//            this.requestPermissions(context, new String[]{Manifest.permission.READ_PHONE_STATE}, REQUEST_READ_PHONE_STATE);
//        }
        String imei = telephonyManager.getDeviceId();
        String simSerial = telephonyManager.getSimSerialNumber();

        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);

        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        String wifiMac = wifiInfo.getMacAddress();

        String androidId = Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);

        String packageName = context.getPackageName();

        //getPackageInfo can raise a PackageManager.NameNotFoundException
        //but considering we just asked for packageName,  if it does then android is all kinds of screwed up
        //so we gonna smother it.
        PackageInfo packageInfo;

        try {
            packageInfo = context.getPackageManager().getPackageInfo(packageName, 0);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("Util", "createDeviceConfig", e);
            //should we log this somewhere more permanent
            return null;
        }

        String versionName = packageInfo.versionName;
        int versionCode = packageInfo.versionCode;

        String imeiHash = null;
        String wifiHash = null;
        String androidIdHash = null;
        String simSerialEnc = null;
        String imeiEnc = null;

        if (imei != null) {
            imeiHash = sha1(imei);
            try {
                imeiEnc = RSAEncrypt(imei);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (wifiMac != null) {
            wifiHash = sha1(wifiMac);
        }

        if (androidId != null) {
            androidIdHash = sha1(androidId);
        }

        if (simSerial != null) {
            try {
                simSerialEnc = RSAEncrypt(simSerial);
                Log.e("SIM SERIAL", simSerialEnc);
            } catch (Exception e) {
                e.printStackTrace();

            }
        }
        DeviceInfo info = new DeviceInfo();

        if (simSerial != null) {
            info.setSimSerialEnc(simSerialEnc);
        }
        info.setSimSerial(simSerial);
        info.setImeiEnc(imeiEnc);
        info.setImeiHash(imeiHash);
        info.setWifiHash(wifiHash);
        info.setAndroidIdHash(androidIdHash);
        info.setApplicationPackageName(packageName);
        info.setApplicationVersionName(versionName);
        info.setApplicationVersionCode(versionCode);

        info.setAndroidBuild(Build.FINGERPRINT);
        info.setAndroidVersion(Build.VERSION.RELEASE);
        info.setAndroidManufacturer(Build.MANUFACTURER);
        info.setAndroidModel(Build.MODEL);

        return info;
    }



    public static String sha1(String s) {
        String hash = null;
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-1");
            byte[] bytes = s.getBytes("UTF-8");
            digest.update(bytes, 0, bytes.length);
            bytes = digest.digest();
            StringBuilder sb = new StringBuilder();
            for (byte b : bytes) {
                sb.append(String.format("%02X", b));
            }
            hash = sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return hash;
    }



    public static byte[] sha256Bytes(String s) {
        byte[] bytes = null;
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            bytes = s.getBytes("UTF-8");
            digest.update(bytes, 0, bytes.length);
            bytes = digest.digest();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return bytes;
    }




    public static String RSAEncrypt(final String plain) throws NoSuchAlgorithmException, NoSuchPaddingException,
            InvalidKeyException, IllegalBlockSizeException, BadPaddingException {

        PublicKey pk = null;

        byte[] publicKey = Base64.decode("MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDM0FZNVs27Bu8ikXq8V" +
                "/08uWq1DYaf+NA5lKPabZ8eNfs09x4Jeih5o+ZHWzDvphvxVg67P8axqpcKw98Ub/CPEbi/FMacl9O3VXu" +
                "Gir09BqgRAtd4Ngi94NygjRhoFPh5aWgJyWdMVzseWqqUPKJnQnLlw7eMOBmH2aa6IDJDEQIDAQAB", Base64.NO_WRAP);

        KeyFactory kf = KeyFactory.getInstance("RSA");
        X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(publicKey);
        try {
            pk = kf.generatePublic(publicKeySpec);
        } catch(InvalidKeySpecException e) {
            e.printStackTrace();
        }

        if (pk != null) {
            Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.ENCRYPT_MODE, pk);
            byte[] encryptedBytes = cipher.doFinal(plain.getBytes());
            String rawPayload = Base64.encodeToString(encryptedBytes, Base64.NO_WRAP);
            StringBuilder _sb = new StringBuilder(rawPayload);
            _sb.insert(0, "DAE"+1+"$");
            return _sb.toString();
        } else {
            return null;
        }




    }








    public static JSONObject getBuildInfoAsJson() {

        String[][] s = new String[][]{
                new String[]{"BOARD", Build.BOARD},
                new String[]{"BOOTLOADER", Build.BOOTLOADER},
                new String[]{"BRAND", Build.BRAND},
                new String[]{"CPU_ABI", Build.CPU_ABI},
                new String[]{"CPU_ABI2", Build.CPU_ABI2},
                new String[]{"DEVICE", Build.DEVICE},
                new String[]{"DISPLAY", Build.DISPLAY},
                new String[]{"FINGERPRINT", Build.FINGERPRINT},
                new String[]{"HARDWARE", Build.HARDWARE},
                new String[]{"HOST", Build.HOST},
                new String[]{"MANUFACTURER", Build.MANUFACTURER},
                new String[]{"MODEL", Build.MODEL},
                new String[]{"PRODUCT", Build.PRODUCT},
                //new String[]{"RADIO", Build.RADIO}, //Deprecated
                new String[]{"SERIAL", Build.SERIAL},
                new String[]{"TAGS", Build.TAGS},
                new String[]{"TIME", String.valueOf(Build.TIME)},
                new String[]{"TYPE", Build.TYPE},
                new String[]{"USER", Build.USER},
                new String[]{"VERSION.CODENAME", Build.VERSION.CODENAME},
                new String[]{"VERSION.INCREMENTAL", Build.VERSION.INCREMENTAL},
                new String[]{"VERSION.RELEASE", Build.VERSION.RELEASE},
                new String[]{"VERSION.SDK_INT", String.valueOf(Build.VERSION.SDK_INT)},
        };

        JSONObject o = new JSONObject();

        try {
            for (String[] i : s) {
                o.put(i[0], i[1]);
            }
        } catch (JSONException e) {
            return null;
        }
        return o;
    }

    public static void setDeviceConfigured(Context context, boolean isConfigured) {
        SharedPreferences settings = context.getSharedPreferences(Constants.PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(Constants.PREFS_IS_CONFIGURED, isConfigured);
        editor.commit();
    }


    public static boolean checkPin(String plainText, String encryptedHash) {
        String[] parts = encryptedHash.split("\\$");

        if (parts.length != 3) {
            return false;
        }

        String algorithm = parts[0];
        String salt = parts[1];
        String hash = parts[2];

        String newHash = sha1(salt + plainText);

        return hash.equalsIgnoreCase(newHash);
    }

    public static byte[] fileToByteArray(String filename) throws FileNotFoundException, IOException {
        final int CHUNK_SIZE = 1024;
        byte[] buf = new byte[CHUNK_SIZE];

        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        FileInputStream fis = new FileInputStream(filename);

        int bytesRead = 0;

        while ((bytesRead = fis.read(buf, 0, CHUNK_SIZE)) >= 0) {
            bao.write(buf, 0, bytesRead);
        }

        return bao.toByteArray();
    }

}
