package com.daelibs.MAP.Data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class DataRepository {

    DatabaseHelper openHelper;
    static SQLiteDatabase db;



    public DataRepository(Context context) {
        openHelper = new DatabaseHelper(context);
        db = openHelper.getWritableDatabase();
    }

    public void intialiseappobject(String packagename){

        ContentValues cv = new ContentValues();
        cv.put(DatabaseHelper.DOWNLOAD_COL_INTIALISED, 1);
        db.update(DatabaseHelper.TABLE_DOWNLOAD,  cv, DatabaseHelper.DOWNLOAD_COL_PACKAGENAME + " = " +  "'" +  packagename+ "'", null) ;

    }
    public void processingappobject(String packagename, Integer intialise){

        ContentValues cv = new ContentValues();
        cv.put(DatabaseHelper.DOWNLOAD_COL_INTIALISED, intialise+1);
        db.update(DatabaseHelper.TABLE_DOWNLOAD,  cv, DatabaseHelper.DOWNLOAD_COL_PACKAGENAME + " = " + "'" + packagename + "'", null) ;

    }

    public void addtoqueue(Appobject appobject) {

        Cursor c = db.rawQuery("SELECT * FROM " + DatabaseHelper.TABLE_DOWNLOAD + " WHERE " + DatabaseHelper.DOWNLOAD_COL_PACKAGENAME + " = " +  "'" + appobject.getPackagename() + "'", null);
        try {
            if (c.moveToFirst()) {
                ContentValues cv = new ContentValues();
                cv.put(DatabaseHelper.DOWNLOAD_COL_PACKAGENAME, appobject.getPackagename());
                cv.put(DatabaseHelper.DOWNLOAD_COL_DOWNLOADURL, appobject.getUpdateurl());
                cv.put(DatabaseHelper.DOWNLOAD_COL_MACSHA, appobject.getMacsha());
                cv.put(DatabaseHelper.DOWNLOAD_COL_PRIORITY, appobject.getPriority());
                cv.put(DatabaseHelper.DOWNLOAD_COL_VERSION, appobject.getVersioname());
                db.update(DatabaseHelper.TABLE_DOWNLOAD,  cv, DatabaseHelper.DOWNLOAD_COL_ID + " = " + c.getInt(0), null) ;
            } else {
                ContentValues cv = new ContentValues();
                cv.put(DatabaseHelper.DOWNLOAD_COL_PACKAGENAME, appobject.getPackagename());
                cv.put(DatabaseHelper.DOWNLOAD_COL_DOWNLOADURL, appobject.getUpdateurl());
                cv.put(DatabaseHelper.DOWNLOAD_COL_MACSHA, appobject.getMacsha());
                cv.put(DatabaseHelper.DOWNLOAD_COL_PRIORITY, appobject.getPriority());
                cv.put(DatabaseHelper.DOWNLOAD_COL_VERSION, appobject.getVersioname());
                long id = db.insert(DatabaseHelper.TABLE_DOWNLOAD, null, cv);
                Log.e("add to q", appobject.getVersioname());
            }

        } finally {
            c.close();
        }


    }


    public Appobject getApptoinstall() {
        Appobject apppackage = new Appobject();

        Cursor c = db.rawQuery("SELECT * FROM " + DatabaseHelper.TABLE_DOWNLOAD + " ORDER BY " + DatabaseHelper.DOWNLOAD_COL_PRIORITY + " ASC LIMIT 1 ", null);
        try {
            if (c.moveToFirst()) {

                    apppackage.setPackagename(c.getString(2));
                    apppackage.setVersioname(c.getString(3));
                    apppackage.setUpdateurl(c.getString(4));
                    apppackage.setMacsha(c.getString(5));
                    apppackage.setInitialise(c.getInt(6));
//                    ContentValues cv = new ContentValues();
//                    cv.put(DatabaseHelper.DOWNLOAD_COL_INTIALISED, 1);
//                    db.update(DatabaseHelper.TABLE_DOWNLOAD,  cv, DatabaseHelper.DOWNLOAD_COL_ID + " = " + c.getInt(0), null) ;

            } else {
                apppackage = null;
                Log.e("gettoinstall", "null");
            }

        } finally {
            c.close();
        }
        return apppackage;
    }


    public void appupdated(Appobject appobject) {
        db.delete(DatabaseHelper.TABLE_DOWNLOAD, DatabaseHelper.DOWNLOAD_COL_VERSION + " = '" + appobject.getVersioname() + "' AND " + DatabaseHelper.DOWNLOAD_COL_PACKAGENAME +  " = '" + appobject.getPackagename() + "'", null);

    }




    public void addlastupdate(String time) {

        Cursor c = db.rawQuery("SELECT * FROM " + DatabaseHelper.TABLE_LAST_UPDATE + " WHERE " + DatabaseHelper.LAST_UPDATE_COL_ID + " = 1 ", null);
        try {
            if (c.moveToFirst()) {

                ContentValues cv = new ContentValues();
                cv.put(DatabaseHelper.LAST_UPDATE_COL_TIME, time);
                db.update(DatabaseHelper.TABLE_LAST_UPDATE,  cv, DatabaseHelper.LAST_UPDATE_COL_ID + " = " + 1, null) ;
            } else {
                ContentValues cv = new ContentValues();
                cv.put(DatabaseHelper.LAST_UPDATE_COL_TIME, time);
                long id = db.insert(DatabaseHelper.TABLE_LAST_UPDATE, null, cv);
            }

        } finally {
            c.close();
        }
    }



    public String getlastupdate() {
        String updatetime;

        Cursor c = db.rawQuery("SELECT * FROM " + DatabaseHelper.TABLE_LAST_UPDATE + " WHERE " + DatabaseHelper.LAST_UPDATE_COL_ID + " = 1 ", null);
        try {
            if (c.moveToFirst()) {
                updatetime=c.getString(1);
            } else {
                updatetime=null;
            }

        } finally {
            c.close();
        }
        return updatetime;
    }


    public String getlastcontact() {
        String contacttime;

        Cursor c = db.rawQuery("SELECT * FROM " + DatabaseHelper.TABLE_LAST_CONTACT + " WHERE " + DatabaseHelper.LAST_CONTACT_COL_ID + " = 1 ", null);
        try {
            if (c.moveToFirst()) {
                contacttime=c.getString(1);
            } else {
                contacttime=null;
            }
        } finally {
            c.close();
        }
        return contacttime;
    }




    public void addlastcontact(String time) {

        Cursor c = db.rawQuery("SELECT * FROM " + DatabaseHelper.TABLE_LAST_CONTACT + " WHERE " + DatabaseHelper.LAST_CONTACT_COL_ID + " = 1 ", null);
        try {
            if (c.moveToFirst()) {
                ContentValues cv = new ContentValues();
                cv.put(DatabaseHelper.LAST_CONTACT_COL_TIME, time);
                db.update(DatabaseHelper.TABLE_LAST_CONTACT,  cv, DatabaseHelper.LAST_CONTACT_COL_ID + " = " + 1, null);
            } else {
                ContentValues cv = new ContentValues();
                cv.put(DatabaseHelper.LAST_CONTACT_COL_TIME, time);
                long id = db.insert(DatabaseHelper.TABLE_LAST_CONTACT, null, cv);
            }

        } finally {
            c.close();
        }
    }



}
