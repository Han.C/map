package com.daelibs.MAP.Data;

public class Constants {
    public static final String FRAMEWORK_VERSION = "20140401_1340";

    public static final String PREFS_NAME = "Snf5Prefs";

    public static final String PREFS_CONFIG_IS_COMPLETE = "config.isComplete";

    public static final String PREFS_FILTER_INTERVAL = "filterInterval";

    public static final String PREFS_LOGGER_SERIAL_NUMBER = "loggerSerialNumber";
    public static final String PREFS_CURRENT_REGISTRATION_SERVICE_INSTANCE_NAME = "currentRegistrationServiceInstanceName";
    public static final String PREFS_IS_CONFIGURED = "deviceIsConfigured";

    public static final String PREFS_INFO_URL = "infoUrl";
    public static final String PREFS_DATA_UPLOAD_URL = "dataUploadUrl";
    public static final String PREFS_GELATI_UPLOAD_URL = "gelatiUploadUrl";
}