package com.daelibs.MAP.Data;

public class ConfigResult {

    public static class Feature {
        public String name;
        public boolean enabled;
    }

    private String dataUploadUrl;
    private String gelatiUploadUrl;
    private String infoUrl;
    private int filterInterval;
    private String serviceInstanceName;
    private Feature[] features;


    public String getDataUploadUrl() {
        return dataUploadUrl;
    }
    public void setDataUploadUrl(String dataUrl) {
        this.dataUploadUrl = dataUrl;
    }

    public String getGelatiUploadUrl() {
        return gelatiUploadUrl;
    }

    public void setGelatiUploadUrl(String gelatiUploadUrl) {
        this.gelatiUploadUrl = gelatiUploadUrl;
    }

    public String getInfoUrl() {
        return infoUrl;
    }
    public void setInfoUrl(String infoUrl) {
        this.infoUrl = infoUrl;
    }
    public int getFilterInterval() {
        return filterInterval;
    }
    public void setFilterInterval(int filterInterval) {
        this.filterInterval = filterInterval;
    }

    public String getServiceInstanceName() {
        return serviceInstanceName;
    }

    public void setServiceInstanceName(String serviceInstanceName) {
        this.serviceInstanceName = serviceInstanceName;
    }

    public void setFeatures(Feature[] features) {
        this.features = features;
    }
    public Feature[] getFeatures() {
        return features;
    }
}
