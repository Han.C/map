package com.daelibs.MAP.Data;

public class DeviceInfo {
    private String imeiHash;
    private String wifiHash;
    private String androidIdHash;
    private String applicationPackageName;
    private String applicationVersionName;
    private int applicationVersionCode;

    private String androidVersion;
    private String androidBuild;
    private String androidManufacturer;
    private String androidModel;
    private String simSerial;

    public String getImeiEnc() {
        return imeiEnc;
    }

    public void setImeiEnc(String imeiEnc) {
        this.imeiEnc = imeiEnc;
    }

    public String getSimSerialEnc() {
        return simSerialEnc;
    }

    public void setSimSerialEnc(String simSerialEnc) {
        this.simSerialEnc = simSerialEnc;
    }
    public String getSimSerial() {
        return simSerial;
    }

    public void setSimSerial(String simSerial) {
        this.simSerial = simSerial;
    }

    private String imeiEnc;
    private String simSerialEnc;

    public String getImeiHash() {
        return imeiHash;
    }
    public void setImeiHash(String imeiHash) {
        this.imeiHash = imeiHash;
    }
    public String getWifiHash() {
        return wifiHash;
    }
    public void setWifiHash(String wifiHash) {
        this.wifiHash = wifiHash;
    }
    public String getAndroidIdHash() {
        return androidIdHash;
    }
    public void setAndroidIdHash(String androidIdHash) {
        this.androidIdHash = androidIdHash;
    }
    public String getApplicationPackageName() {
        return applicationPackageName;
    }
    public void setApplicationPackageName(String applicationPackageName) {
        this.applicationPackageName = applicationPackageName;
    }
    public String getApplicationVersionName() {
        return applicationVersionName;
    }
    public void setApplicationVersionName(String applicationVersionName) {
        this.applicationVersionName = applicationVersionName;
    }
    public int getApplicationVersionCode() {
        return applicationVersionCode;
    }
    public void setApplicationVersionCode(int applicationVersionCode) {
        this.applicationVersionCode = applicationVersionCode;
    }
    public String getAndroidVersion() {
        return androidVersion;
    }
    public void setAndroidVersion(String androidVersion) {
        this.androidVersion = androidVersion;
    }
    public String getAndroidBuild() {
        return androidBuild;
    }
    public void setAndroidBuild(String androidBuild) {
        this.androidBuild = androidBuild;
    }
    public String getAndroidManufacturer() {
        return androidManufacturer;
    }
    public void setAndroidManufacturer(String androidManufacturer) {
        this.androidManufacturer = androidManufacturer;
    }
    public String getAndroidModel() {
        return androidModel;
    }
    public void setAndroidModel(String androidModel) {
        this.androidModel = androidModel;
    }


}
