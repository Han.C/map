package com.daelibs.MAP.Data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class DatabaseHelper extends SQLiteOpenHelper {



    public static final String DATABASE_NAME = "MAPdatabase";

    private static final int DATABASE_VERSION = 3;

    public static final String TABLE_DOWNLOAD = "download_queue";

    public static final String DOWNLOAD_COL_ID = "id";
    public static final String DOWNLOAD_COL_PRIORITY = "priority";
    public static final String DOWNLOAD_COL_PACKAGENAME = "packagename";
    public static final String DOWNLOAD_COL_VERSION = "version";
    public static final String DOWNLOAD_COL_DOWNLOADURL = "url";
    public static final String DOWNLOAD_COL_MACSHA = "macsha";
    public static final String DOWNLOAD_COL_INTIALISED = "download_time";

    public static final String TABLE_LAST_UPDATE = "last_update_table";
    public static final String TABLE_LAST_CONTACT = "last_contact_table";

    public static final String LAST_UPDATE_COL_ID = "id";
    public static final String LAST_UPDATE_COL_TIME = "lastupdate_time";

    public static final String LAST_CONTACT_COL_ID = "id";
    public static final String LAST_CONTACT_COL_TIME = "lastcontact_time";


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    private String createIndexSql(String tableName, String columnName) {
        return "CREATE INDEX " + tableName + "_" + columnName +"_idx " +
                "on " + tableName + "(" + columnName + ");";
    }



    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("CREATE TABLE " + TABLE_DOWNLOAD + " (" +
                DOWNLOAD_COL_ID + " integer primary key autoincrement, " +
                DOWNLOAD_COL_PRIORITY + " integer, " +
                DOWNLOAD_COL_PACKAGENAME + " text, " +
                DOWNLOAD_COL_VERSION + " text, " +
                DOWNLOAD_COL_DOWNLOADURL + " text, " +
                DOWNLOAD_COL_MACSHA + " text, " +
                DOWNLOAD_COL_INTIALISED + " integer default 0 " +
                ");");

        db.execSQL(createIndexSql(TABLE_DOWNLOAD, DOWNLOAD_COL_PACKAGENAME));


        db.execSQL("CREATE TABLE " + TABLE_LAST_CONTACT + " (" +
                LAST_CONTACT_COL_ID + " integer primary key autoincrement, " +
                LAST_CONTACT_COL_TIME + " text " +
                ");");

        db.execSQL("CREATE TABLE " + TABLE_LAST_UPDATE + " (" +
                LAST_UPDATE_COL_ID + " integer primary key autoincrement, " +
                LAST_UPDATE_COL_TIME + " text " +
                ");");





    }



    public void applyVersion3(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_LAST_CONTACT + " (" +
                LAST_CONTACT_COL_ID + " integer primary key autoincrement, " +
                LAST_CONTACT_COL_TIME + " text " +
                ");");

        db.execSQL("CREATE TABLE " + TABLE_LAST_UPDATE + " (" +
                LAST_UPDATE_COL_ID + " integer primary key autoincrement, " +
                LAST_UPDATE_COL_TIME + " text " +
                ");");

        db.execSQL("ALTER TABLE " + TABLE_DOWNLOAD + " " +
                "ADD COLUMN " + DOWNLOAD_COL_INTIALISED + " integer default 0 ");
    }





    @Override
    public void onUpgrade(SQLiteDatabase db, int currentVersion, int latestVersion) {
        if (currentVersion < latestVersion) {
            //run all the required applyVersionN methods:
            for (int v = currentVersion + 1; v <= latestVersion; v++) {
                String methodName = "applyVersion" + v;
                Method applyMethod = null;
                try {
                    applyMethod = this.getClass().getMethod(methodName, SQLiteDatabase.class);
                } catch (NoSuchMethodException e) {
                    throw new RuntimeException("DatabaseUpgrade " + methodName + " method not found", e);
                }

                try {
                    applyMethod.invoke(this, db);
                } catch (IllegalAccessException e) {
                    throw new RuntimeException("invoking methodName", e);
                } catch (InvocationTargetException e) {
                    throw new RuntimeException("invoking methodName", e);
                }
            }
        }
    }
}
