package com.daelibs.MAP.Data;

public class Appobject {

    String packagename;
    String versioname;
    String updateurl;
    String macsha;
    int priority;
    int initialise;


    public String getMacsha(){
       return macsha;
    }
    public void setMacsha(String macsha){
        this.macsha = macsha;
    }

    public String getPackagename(){
        return packagename;
    }
    public void setPackagename(String packagename){
        this.packagename = packagename;
    }

    public String getVersioname(){
        return versioname;
    }
    public void setVersioname(String versioname){
        this.versioname = versioname;
    }

    public String getUpdateurl(){
        return updateurl;
    }
    public void setUpdateurl(String updateurl){
        this.updateurl = updateurl;
    }

    public int getPriority(){
        return priority;
    }
    public void setPriority(int priority){
        this.priority = priority;
    }
    public int getInitialise(){
        return initialise;
    }
    public void setInitialise(int priority){
        this.priority = initialise;
    }
}
