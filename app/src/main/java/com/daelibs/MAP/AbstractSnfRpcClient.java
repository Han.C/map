package com.daelibs.MAP;



import android.util.Log;

import org.apache.http.conn.ConnectTimeoutException;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public abstract class AbstractSnfRpcClient {

    public abstract static class SnfRpcException extends Exception {
        public SnfRpcException(String message) {
            super(message);
        }
        public SnfRpcException(String message, Throwable cause) {
            super(message, cause);
        }
    }

    public static class ProtocolException extends SnfRpcException {
        public ProtocolException(String message) {
            super(message);
        }
        public ProtocolException(String message, Throwable cause) {
            super(message, cause);
        }
    }

    public static class ServiceException extends SnfRpcException {
        public ServiceException(String message) {
            super(message);
        }
        public ServiceException(String message, Throwable cause) {
            super(message, cause);
        }
    }

    private String serviceBaseAddress;
    private String serviceDescription;
    private boolean encryptBody = false;

    private static final String LOGTAG = "SnfRpcClient";

    public AbstractSnfRpcClient(String serviceDescription, String serviceAddress) {
        this.serviceBaseAddress = serviceAddress;
        this.serviceDescription = serviceDescription;
    }

    public void setEncryptBody(boolean encryptBody) {
        this.encryptBody = encryptBody;
    }

    protected JSONObject doRequest(String action, JSONObject requestBody) throws SnfRpcException, IOException {

        String data;
        StringBuilder output = new StringBuilder();
        String body;

        try {
            JSONObject request = new JSONObject();
            request.put("action", action);
            request.put("body", requestBody);
            body = request.toString();
        } catch (JSONException e) {
            throw new ProtocolException("error encoding request body");
        }

        Log.i(LOGTAG, serviceDescription + "doRequest - " + serviceBaseAddress);

        String contentType = "application/json";

        if (encryptBody) {
            body = Security.encrypt(body);
            contentType = "text/plain";
        }

        try {
            URL url = new URL(serviceBaseAddress);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            try {
                connection.setRequestProperty("Content-Type", contentType);
                connection.setRequestProperty("Content-Length", String.valueOf(body.length()));
                connection.setRequestProperty("Connection", "close"); //disables connection reuse

                Log.i(LOGTAG, serviceDescription + " body length " + body.length());

                connection.setDoOutput(true);
                connection.setFixedLengthStreamingMode(body.length());
                connection.setConnectTimeout(10000);
                connection.setReadTimeout(10000);

                BufferedWriter out = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream()));

                out.append(body);
                out.flush();
                out.close();

                int status = connection.getResponseCode();

                if (status != HttpURLConnection.HTTP_OK) {
                    throw new ProtocolException(String.format("Invalid HTTP Status: %d %s", status, connection.getResponseMessage()));
                }

                BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));

                while ((data = in.readLine()) != null) {
                    output.append(data);
                }

                Log.i(LOGTAG, output.toString());

                JSONObject response = new JSONObject(output.toString());
                Log.d("RESPONSE JSON", response.toString());
                JSONObject error = response.optJSONObject("error");

                if (error != null) {
                    String message = error.optString("message");
                    if (message ==  null) {
                        throw new ProtocolException("Service Exception without message");
                    } else {
                        throw new ServiceException(message);
                    }
                }

                // handle "body" is only a string, not JSON object
                JSONObject jsonObject = response.optJSONObject("body");
                if (jsonObject == null) {
                    String bodyString = response.optString("body");
                    if (bodyString != null) {
                        // replace {\"} with {"}
                        return new JSONObject(bodyString.replaceAll("\\\"", "\""));
                    } else {
                        throw new IllegalStateException("body string is null");
                    }
                } else {
                    return jsonObject;
                }

            }catch (ConnectTimeoutException t) {
                Log.e(LOGTAG, t.toString());
                return null;
            } finally {
                connection.disconnect();
            }
        } catch (JSONException e) {
            Log.e(LOGTAG, e.toString());
            throw new ProtocolException("Unable to parse response");
        }
    }
}
