package com.daelibs.MAP;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.StrictMode;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.daelibs.MAP.Data.Appobject;
import com.daelibs.MAP.Data.DataRepository;
import com.daelibs.MAP.Data.DeviceInfo;
import com.daelibs.MAP.UpdateService.UpdateClient;
import com.daelibs.MAP.UpdateService.UpdateClientException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

public class WebService extends Service {

    public WifiManager wifiManager;
    public UpdateClient updateClient;
    public WifiManager.WifiLock wl;
    public PowerManager.WakeLock wakeLock;

    @Override
    public IBinder onBind(Intent intent) {




        return null;
    }
    @Override
    public void onCreate() {

        if (Build.VERSION.SDK_INT >= 26) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                String NOTIFICATION_CHANNEL_ID = "com.daelibs.MAP";
                String channelName = "My Background Service";
                NotificationChannel chan = new NotificationChannel(NOTIFICATION_CHANNEL_ID, channelName, NotificationManager.IMPORTANCE_NONE);
                chan.setLightColor(Color.BLUE);
                chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
                NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                assert manager != null;
                manager.createNotificationChannel(chan);
                NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
                Notification notification = notificationBuilder.setOngoing(true)
                        .setContentTitle("App is running in background")
                        .setPriority(NotificationManager.IMPORTANCE_MIN)
                        .setCategory(Notification.CATEGORY_SERVICE)
                        .build();
                startForeground(1,notification);
            }else{
                startForeground(1,new Notification());
            }
        }

        PowerManager powerManager = (PowerManager) getApplicationContext().getSystemService(POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                "myapp:MyWakelockTag");
        if ((wakeLock != null) && (wakeLock.isHeld() == false)) {
            wakeLock.acquire();
        }
        WifiManager wm = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        wl = wm.createWifiLock(WifiManager.WIFI_MODE_FULL_HIGH_PERF, "myId");
        if ((wl != null) && (wl.isHeld() == false)) {
            wl.acquire();
        }




        Log.e("service","started");
        final Handler handler = new Handler();

        final Handler handler2 = new Handler();
        final int delay2 = 60000; //milliseconds

        final Handler handler3 = new Handler();

        final int delay = 900000; //milliseconds
//        final int delay = 30000; //milliseconds
        wifiManager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
        if(Build.VERSION.SDK_INT>=24){
            try{
                Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                m.invoke(null);
            }catch(Exception e){
                e.printStackTrace();
            }
        }





        handler2.postDelayed(new Runnable(){
            public void run(){
                Log.e("Installing handler","Running");
                final PackageManager pm = getPackageManager();
                List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);
                DataRepository repo = new DataRepository(getApplicationContext());
                Appobject appObject = repo.getApptoinstall();
                if(appObject!=null) {
                    Log.e("Apptoinstall", appObject.getPackagename());
                    Boolean updateapp = true;

                    for (ApplicationInfo packageInfo : packages) {
                        if (packageInfo.packageName.equals(appObject.getPackagename())) {
                            try {
                                PackageInfo verinfo = getPackageManager().getPackageInfo(packageInfo.packageName, 0);
                                Log.e("Package", "Version : " + verinfo.versionName);
                                Log.e("Package", "Version : " + appObject.getVersioname());
                                if (verinfo.versionName.equals(appObject.getVersioname())) {
                                    updateapp = false;
                                }
                            } catch (PackageManager.NameNotFoundException e) {
                                e.printStackTrace();
                            }
                        }
                    }


                    if (updateapp) {
                        if(appObject.getInitialise()<6 && appObject.getInitialise()>0){
                            repo.processingappobject(appObject.getPackagename(),appObject.getInitialise());
                        }else{
                            Updateanapp(appObject.getUpdateurl(), appObject.getMacsha(),appObject.getPackagename() );
                            repo.intialiseappobject(appObject.getPackagename());
                        }
                    } else {
                        try {
                            sendappvers(appObject.getVersioname(), appObject.getPackagename());
                        } catch (JSONException e) {
                            Log.e("JSON EXCEPTION:" , e.toString());
                            e.printStackTrace();
                        } catch (IOException e) {
                            Log.e("IO EXCEPTION:" , e.toString());
                            e.printStackTrace();
                        } catch (AbstractSnfRpcClient.SnfRpcException e) {
                            Log.e("SnfRpcException:" , e.toString());
                        }

                        repo.appupdated(appObject);
                        Appobject newappObject = repo.getApptoinstall();

                        if(newappObject!=null) {
                            Log.e("Apptoinstall", newappObject.getPackagename());
                            Boolean newupdateapp = true;
                            for (ApplicationInfo packageInfo : packages) {
                                if (packageInfo.packageName.equals(newappObject.getPackagename())) {
                                    try {
                                        PackageInfo verinfo = getPackageManager().getPackageInfo(packageInfo.packageName, 0);
                                        Log.e("Package", "Version : " + verinfo.versionName);
                                        if (verinfo.versionName.equals(newappObject.getVersioname())) {
                                            newupdateapp = false;

                                        }
                                    } catch (PackageManager.NameNotFoundException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                            if (newupdateapp) {
                                if(newappObject.getInitialise()<3 && newappObject.getInitialise()>0){
                                    repo.processingappobject(newappObject.getPackagename(),newappObject.getInitialise());
                                }else {
                                    Updateanapp(newappObject.getUpdateurl(), newappObject.getMacsha(), newappObject.getPackagename());
                                    repo.intialiseappobject(newappObject.getPackagename());
                                }
                            } else {
                                repo.appupdated(newappObject);
                            }
                        }
                    }
                }

                handler2.postDelayed(this, delay2);
            }
        }, delay2);



        handler3.postDelayed(new Runnable(){
            public void run(){
                if (ContextCompat.checkSelfPermission(getApplicationContext(),
                        Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getApplicationContext(), "MAP Permission not granted", Toast.LENGTH_LONG).show();
                }else{
                     Log.e("intiating","process");
                     doInit();
                }
            }
        }, 15000);



        handler.postDelayed(new Runnable(){
            public void run(){

                Log.e("intiating","first handler");

                if (ContextCompat.checkSelfPermission(getApplicationContext(),
                        Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getApplicationContext(), "MAP Permission not granted", Toast.LENGTH_LONG).show();
                }else{
                    Log.e("intiating","process");
                    doInit();
                }
                handler.postDelayed(this, delay);
            }
        }, delay);







    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }






    @Override
    public void onDestroy() {
        super.onDestroy();


        if ((wakeLock != null) && (wakeLock.isHeld() == false)) {
            wakeLock.release();
        }
        if ((wl != null) && (wl.isHeld() == false)) {
            wl.release();
        }


        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction("restartservice");
        broadcastIntent.setClass(this, restartservice.class);
        this.sendBroadcast(broadcastIntent);


    }










    private void enableWifi() {
        wifiManager.setWifiEnabled(true);
        for (int i = 0; i < 20 && !wifiManager.isWifiEnabled(); i++) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException ex) {
                return;
            }
        }
    }



    private void writeRegFile(String serialNumber) {
        File file = new File(getExternalFilesDir(null), "reg.info");

        FileWriter fw = null;
        try {

            TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
            if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                Log.e("permission"," not granted");
            } else {
                Log.e("permission","granted");
            }
            String imei = telephonyManager.getDeviceId();
            if (imei == null) {
                imei = "";
            }

            WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            WifiInfo wifiInfo = wifiManager.getConnectionInfo();
            String wifiMac = "";
            if (wifiInfo != null) {
                wifiMac = wifiInfo.getMacAddress();
                if (wifiMac == null) {
                    wifiMac = "";
                }
            }

            fw = new FileWriter(file.getCanonicalPath());
            fw.write(serialNumber + ",");
            fw.write(imei + ",");
            fw.write(wifiMac + "\r\n");
        } catch (Exception ex) {
            Toast.makeText(this, "Unable to write reg file", Toast.LENGTH_LONG).show();
        } finally {
            if (fw != null) {
                try {
                    fw.close();
                } catch (IOException ex) {
                    //
                }
            }
        }
    }



    @SuppressLint("StaticFieldLeak")
    private void doInit() {

        final String url = getResources().getString(R.string.registration_service_url);

        new AsyncTask<Void, String, Void>() {

            String error = null;

            @Override
            public Void doInBackground(Void... args) {

                boolean disableWifi = false;


                    publishProgress("Attempting to register a serial number");

                    //Make sure wifi is enabled so we can access the Wifi Mac Address
                    if (!wifiManager.isWifiEnabled()) {
                        publishProgress("Enabling Wifi");
                        enableWifi();
                        disableWifi = true;
                    }

                    try {
                        if (!activate()) {
                            error = "Unable to retrieve a serial number";
                            return null;
                        }

                    } catch (AbstractSnfRpcClient.SnfRpcException e) {
                        error = e.getMessage();
                    } catch (JSONException e) {
                        error = e.toString();
                    } catch (IOException e) {
                        error = "Network Error " + e.getMessage();
                    }
                    if (disableWifi) {
                        wifiManager.setWifiEnabled(false);
                    }

                    if (error != null) {
                        return null;
                    }



                final String loggerSerialNumber = Util.getSerial(getApplicationContext());

                publishProgress("Serial number obtained");

//                ConfigResult configResult = Util.getRegistrationConfig(getApplicationContext(), url, loggerSerialNumber);

//                if (configResult == null) {
//                    error = "Unable to retrieve device configuration";
//                } else {
////                    DataRepository repo = new DataRepository(getApplicationContext());
//
//                    if (configResult.getInfoUrl() == null) {
//                        error = "Please assign this device to a SeeknFind Instance\n\nDevice: " + loggerSerialNumber;
//                    } else {
//                        Log.e("sitecode", configResult.getInfoUrl());
//
//
//                    }
//
//                    if (configResult.getGelatiUploadUrl() != null) {
//                        SharedPreferences p = getSharedPreferences(Constants.PREFS_NAME, 0);
//                        SharedPreferences.Editor e = p.edit();
//                        e.putString(Constants.PREFS_GELATI_UPLOAD_URL, configResult.getGelatiUploadUrl());
//                        e.commit();
//                    }
//
//                    try {
//                        RegistrationClient s = new RegistrationClient(url);
//                        JSONObject settingsJson = s.getSettings(loggerSerialNumber);
//                        RemoteSettings remoteSettings = new RemoteSettings(getApplicationContext());
//                        remoteSettings.applyJson(settingsJson);
//                    } catch (JSONException e) {
//                        error = "Error parsing settings";
//                    } catch (AbstractSnfRpcClient.SnfRpcException e) {
//                        error = "RPC Error";
//                    } catch (IOException e) {
//                        error = "Network Error retrieving settings";
//                    }
//
//                }




                return null;
            }

            @Override
            public void onProgressUpdate(String... args) {
                //setStatus(args[0]);
            }

            @Override
            public void onPostExecute(Void result) {
                try {

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("Exception",e.toString());
                }
                if (error != null) {
                    Log.e("ERROR", error);
                }

            }

        }.execute();

    }



    private void sendappvers (final String appversion, final String appname) throws JSONException, IOException, AbstractSnfRpcClient.SnfRpcException {




        new AsyncTask<Void, String, Void>() {

            String error = null;

            @Override
            public Void doInBackground(Void... args) {


                String url = getResources().getString(R.string.registration_service_url);
                RegistrationClient s = new RegistrationClient(url);
                RegistrationClient.GetSerialResult result = null;
                DeviceInfo deviceInfo = Util.createDeviceConfig(getApplicationContext());
                try {
                    s.sendappvers(deviceInfo, appversion, appname);
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (AbstractSnfRpcClient.SnfRpcException e) {
                    e.printStackTrace();
                }
                return null;
            }

            protected void onPostExecute(Exception e) {


                if (e != null) {
                    Log.e("Exception", e.toString());
                    if (e instanceof UpdateClientException) {
                        Toast.makeText(getApplicationContext(), "Update Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "Update Error", Toast.LENGTH_LONG).show();
                    }
                }
            }
        }.execute();
    }







    private boolean activate () throws JSONException, IOException, AbstractSnfRpcClient.SnfRpcException {
        String url = getResources().getString(R.string.registration_service_url);
        RegistrationClient s = new RegistrationClient(url);
        RegistrationClient.GetSerialResult result = null;

        String hintSite = null;
        String hintConfiguration = null;

        DeviceInfo deviceInfo = Util.createDeviceConfig(this);

        result = s.activate(deviceInfo, hintSite, hintConfiguration);




        final PackageManager pm = getPackageManager();

        List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);




        DataRepository repo = new DataRepository(getApplicationContext());



        if (result != null) {
            if(result.getresulttype().equals("2")) {
                onClickDownloadAndInstall(result.getupdateurl(), result.getMacsha());
                Util.setLoggerSerialNumber(this, result.getSerialNumber());
                Intent intent = new Intent(MainActivity.RECEIVER_INTENT);
                intent.putExtra(MainActivity.RECEIVER_MESSAGE,  result.getSerialNumber());
                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                Calendar calander = Calendar.getInstance();
                int cDay = calander.get(Calendar.DAY_OF_MONTH);
                int cMonth = calander.get(Calendar.MONTH) + 1;
                int cYear = calander.get(Calendar.YEAR);
                String selectedMonth = "" + cMonth;
                String selectedYear = "" + cYear;
                String sday = "" + cDay;
                String cHour = String.valueOf(calander.get(Calendar.HOUR_OF_DAY));
                String cMinute = String.valueOf(calander.get(Calendar.MINUTE));
                String cSecond = String.valueOf(calander.get(Calendar.SECOND));
                if(cHour.length()==1){
                    cHour = "0"+cHour;
                }
                if(cMinute.length()==1){
                    cMinute = "0"+cMinute;
                }
                if(cSecond.length()==1){
                    cSecond = "0"+cSecond;
                }
                String lastupdatestring = sday + "/" + selectedMonth + "/" + selectedYear + "  "+ cHour + ":" + cMinute + ":"+ cSecond;
                repo.addlastcontact(lastupdatestring);
                return true;
            }
            else if(result.getresulttype().equals("66")){
                Util.setLoggerSerialNumber(this, result.getSerialNumber());
                Intent intent = new Intent(MainActivity.RECEIVER_INTENT);
                intent.putExtra(MainActivity.RECEIVER_MESSAGE,  result.getSerialNumber());
                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                Calendar calander = Calendar.getInstance();
                int cDay = calander.get(Calendar.DAY_OF_MONTH);
                int cMonth = calander.get(Calendar.MONTH) + 1;
                int cYear = calander.get(Calendar.YEAR);
                String selectedMonth = "" + cMonth;
                String selectedYear = "" + cYear;
                String sday = "" + cDay;
                String cHour = String.valueOf(calander.get(Calendar.HOUR_OF_DAY));

                String cMinute = String.valueOf(calander.get(Calendar.MINUTE));
                String cSecond = String.valueOf(calander.get(Calendar.SECOND));
                if(cHour.length()==1){
                    cHour = "0"+cHour;
                }
                if(cMinute.length()==1){
                    cMinute = "0"+cMinute;
                }
                if(cSecond.length()==1){
                    cSecond = "0"+cSecond;
                }


                String lastupdatestring = sday + "/" + selectedMonth + "/" + selectedYear + "  "+ cHour + ":" + cMinute + ":"+ cSecond;
                repo.addlastcontact(lastupdatestring);




                String command = result.getCommand();
                List<String> cmds = Arrays.asList(command.split("\\s*,\\s*"));
                UpdateClient.runCommand(getApplicationContext(), cmds);

                return true;
            }



            else if(result.getresulttype().equals("0")){
                Util.setLoggerSerialNumber(this, result.getSerialNumber());
                Intent intent = new Intent(MainActivity.RECEIVER_INTENT);
                intent.putExtra(MainActivity.RECEIVER_MESSAGE,  result.getSerialNumber());
                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);

                Calendar calander = Calendar.getInstance();
                int cDay = calander.get(Calendar.DAY_OF_MONTH);
                int cMonth = calander.get(Calendar.MONTH) + 1;
                int cYear = calander.get(Calendar.YEAR);
                String selectedMonth = "" + cMonth;
                String selectedYear = "" + cYear;
                String sday = "" + cDay;
                String cHour = String.valueOf(calander.get(Calendar.HOUR_OF_DAY));
                String cMinute = String.valueOf(calander.get(Calendar.MINUTE));
                String cSecond = String.valueOf(calander.get(Calendar.SECOND));
                if(cHour.length()==1){
                    cHour = "0"+cHour;
                }
                if(cMinute.length()==1){
                    cMinute = "0"+cMinute;
                }
                if(cSecond.length()==1){
                    cSecond = "0"+cSecond;
                }
                String lastupdatestring = sday + "/" + selectedMonth + "/" + selectedYear + "  "+ cHour + ":" + cMinute + ":"+ cSecond;
                repo.addlastcontact(lastupdatestring);


                writeRegFile(result.getSerialNumber());
                return true;
            }
            else if(result.getresulttype().equals("3")){


                JSONArray apps = result.getApps();
                for(int i=0; i<apps.length(); i++) {
                    try {
                        Appobject apppackage = new Appobject();

                        JSONObject appvers = apps.getJSONObject(i);
                        String packagename = appvers.getString("app__package_name");
                        apppackage.setPackagename(packagename);
                        String versioname = appvers.getString("updateversion__version");
                        apppackage.setVersioname(versioname);
                        String updateurl = appvers.getString("updateversion__updateurl");
                        apppackage.setUpdateurl(updateurl);
                        String macsha = appvers.getString("updateversion__macsha");
                        apppackage.setMacsha(macsha);
                        String priority = appvers.getString("precedence");
                        apppackage.setPriority(Integer.parseInt(priority));
                        Boolean updateapp = true;

                        for (ApplicationInfo packageInfo : packages) {
                            if (packageInfo.packageName.equals(packagename)) {
                                try {
                                    PackageInfo verinfo = getPackageManager().getPackageInfo(packageInfo.packageName, 0);
                                    Log.e("Package", "Version : " + verinfo.versionName);
                                    if (verinfo.versionName.equals(versioname)) {
                                        updateapp = false;
                                    }
                                } catch (PackageManager.NameNotFoundException e) {
                                    e.printStackTrace();
                                }

                            }
                        }
                        if (updateapp) {
                            repo.addtoqueue(apppackage);
                            Log.e("App Package",apppackage.getPackagename());
                        }else{
                            sendappvers(versioname,packagename);
                        }

                    } catch (Exception e) {
                        Log.e("Error here",e.toString());
                    }

                }
//                Log.e("Apps",result.getApps());
                Util.setLoggerSerialNumber(this, result.getSerialNumber());
                Intent intent = new Intent(MainActivity.RECEIVER_INTENT);
                intent.putExtra(MainActivity.RECEIVER_MESSAGE,  result.getSerialNumber());
                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                Calendar calander = Calendar.getInstance();
                int cDay = calander.get(Calendar.DAY_OF_MONTH);
                int cMonth = calander.get(Calendar.MONTH) + 1;
                int cYear = calander.get(Calendar.YEAR);
                String selectedMonth = "" + cMonth;
                String selectedYear = "" + cYear;
                String sday = "" + cDay;
                String cHour = String.valueOf(calander.get(Calendar.HOUR_OF_DAY));
                String cMinute = String.valueOf(calander.get(Calendar.MINUTE));
                String cSecond = String.valueOf(calander.get(Calendar.SECOND));
                if(cHour.length()==1){
                    cHour = "0"+cHour;
                }
                if(cMinute.length()==1){
                    cMinute = "0"+cMinute;
                }
                if(cSecond.length()==1){
                    cSecond = "0"+cSecond;
                }
                String lastupdatestring = sday + "/" + selectedMonth + "/" + selectedYear + "  "+ cHour + ":" + cMinute + ":"+ cSecond;
                repo.addlastcontact(lastupdatestring);

                return true;
            }else{
                Util.setLoggerSerialNumber(this, result.getSerialNumber());
                Intent intent = new Intent(MainActivity.RECEIVER_INTENT);
                intent.putExtra(MainActivity.RECEIVER_MESSAGE,  result.getSerialNumber());
                Calendar calander = Calendar.getInstance();
                int cDay = calander.get(Calendar.DAY_OF_MONTH);
                int cMonth = calander.get(Calendar.MONTH) + 1;
                int cYear = calander.get(Calendar.YEAR);
                String selectedMonth = "" + cMonth;
                String selectedYear = "" + cYear;
                String sday = "" + cDay;
                String cHour = String.valueOf(calander.get(Calendar.HOUR_OF_DAY));
                String cMinute = String.valueOf(calander.get(Calendar.MINUTE));
                String cSecond = String.valueOf(calander.get(Calendar.SECOND));
                if(cHour.length()==1){
                    cHour = "0"+cHour;
                }
                if(cMinute.length()==1){
                    cMinute = "0"+cMinute;
                }
                if(cSecond.length()==1){
                    cSecond = "0"+cSecond;
                }
                String lastupdatestring = sday + "/" + selectedMonth + "/" + selectedYear + "  "+ cHour + ":" + cMinute + ":"+ cSecond;
                repo.addlastcontact(lastupdatestring);
                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                return true;
            }

        } else {
            return false;
        }
    }




    @SuppressLint("StaticFieldLeak")
    private void onClickDownloadAndInstall(final String url, final String macsha) {

        final UpdateClient.ProgressListener progressListener = new UpdateClient.ProgressListener() {
            @Override
            public void onProgressUpdate(int percent) {

            }
        };

        new AsyncTask<Void, Void, Exception>() {
            @Override
            protected Exception doInBackground(Void...params) {
                try {

                    UpdateClient.doUpdate(getApplicationContext(), url, progressListener, macsha);

                } catch (UpdateClientException e) {
                    Log.e("Exception", e.toString());
                    return e;

                } catch (Exception e) {
                    Log.e("Exception", e.toString());
                    return e;

                }
                return null;
            }

            @Override
            protected void onPostExecute(Exception e) {


                if (e != null) {
                    Log.e("Exception", e.toString());
                    if (e instanceof UpdateClientException) {
                        Toast.makeText(getApplicationContext(), "Update Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "Update Error", Toast.LENGTH_LONG).show();
                    }
                }

//                Calendar calander = Calendar.getInstance();
//                int cDay = calander.get(Calendar.DAY_OF_MONTH);
//                int cMonth = calander.get(Calendar.MONTH) + 1;
//                int cYear = calander.get(Calendar.YEAR);
//                String selectedMonth = "" + cMonth;
//                String selectedYear = "" + cYear;
//                String sday = "" + cDay;
//                String cHour = String.valueOf(calander.get(Calendar.HOUR_OF_DAY));
//                String cMinute = String.valueOf(calander.get(Calendar.MINUTE));
//                String cSecond = String.valueOf(calander.get(Calendar.SECOND));
//                if(cHour.length()==1){
//                    cHour = "0"+cHour;
//                }
//                if(cMinute.length()==1){
//                    cMinute = "0"+cMinute;
//                }
//                if(cSecond.length()==1){
//                    cSecond = "0"+cSecond;
//                }
//                String lastupdatestring = sday + "/" + selectedMonth + "/" + selectedYear + "  "+ cHour + ":" + cMinute + ":"+ cSecond;
//                DataRepository repo = new DataRepository(getApplicationContext());
//                repo.addlastupdate(lastupdatestring);


            }
        }.execute();
    }



    @SuppressLint("StaticFieldLeak")
    private void Updateanapp(final String url, final String macsha, final String appname) {

        final UpdateClient.ProgressListener progressListener = new UpdateClient.ProgressListener() {
            @Override
            public void onProgressUpdate(int percent) {

            }
        };

        new AsyncTask<Void, Void, Exception>() {
            @Override
            protected Exception doInBackground(Void...params) {
                try {

                    UpdateClient.doUpdate(getApplicationContext(), url, progressListener, macsha);

                } catch (UpdateClientException e) {
                    Log.e("Exception", e.toString());
                    return e;

                } catch (Exception e) {
                    Log.e("Exception", e.toString());
                    return e;

                }
                return null;
            }

            @Override
            protected void onPostExecute(Exception e) {
                if (e != null) {
                    Log.e("Exception", e.toString());
                    if (e instanceof UpdateClientException) {
                        Toast.makeText(getApplicationContext(), "Update Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "Update Error", Toast.LENGTH_LONG).show();
                    }
                }else{

                    Intent launchIntent = getPackageManager().getLaunchIntentForPackage(appname);
                    if (launchIntent != null) {
                        startActivity(launchIntent);
                    }


                }
            }
        }.execute();
    }



//    private static void sendMessageToActivity(String msg) {
//        Intent intent = new Intent("GPSLocationUpdates");
//        // You can also include some extra data.
//        intent.putExtra("Status", msg);
//
//        LocalBroadcastManager.getInstance().sendBroadcast(intent);
//    }



}
