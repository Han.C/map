package com.daelibs.MAP;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

public class RemoteSettings {

    private static String PREFS_NAME = "RemoteSettings";

    private SharedPreferences sharedPreferences;

    public RemoteSettings(Context c) {
        sharedPreferences = c.getSharedPreferences(PREFS_NAME, 0);
    }

    public SharedPreferences getSharedPreferences() {
        return sharedPreferences;
    }

    public void applyJson(JSONObject settingsJson) throws JSONException {
        SharedPreferences.Editor e = sharedPreferences.edit();

        String bools[] = {
                "batteryAlert",
                "bleBeacon",
                "duressAlarm",
                "editPoint",
                "essentials",
                "fencedLocation",
                "gelati",
                "incidentGps",
                "loneWorker",
                "loneWorkerStandalone",
                "manDown",
                "myDetails",
                "nfcScan",
                "passwordlessLogin",
                "ptt",
                "pushAlert",
                "sendPhotosWifi",
                "showSimSetup",
                "showPhoneDialer",
                "showMessages",
                "MultiplePictures"
        };

        String ints[] = {
                "rssiCompensation"
        };

        // String floats[] = {};
        // String strings[] = {} ;

        for (String k : bools) {
            try {
                e.putBoolean(k, settingsJson.getBoolean(k));
            }catch(Exception  er){
                e.putBoolean(k, false);
            }
        }

        for (String k : ints) {
            e.putInt(k, settingsJson.getInt(k));
        }

        e.putString("remoteConfig", settingsJson.toString());

        e.commit();

    }

    public boolean getBatteryAlert() {
        return sharedPreferences.getBoolean("batteryAlert", false);
    }

    public boolean getBleBeacon () {
        return sharedPreferences.getBoolean("bleBeacon" , false);
    }

    public boolean getDuressAlarm() {
        return sharedPreferences.getBoolean("duressAlarm", false);
    }

    public boolean getEditPoint() {
        return sharedPreferences.getBoolean("editPoint", false);
    }

    public boolean getEssentials() {
        return sharedPreferences.getBoolean("essentials" , false);
    }

    public boolean getFencedLocation() {
        return sharedPreferences.getBoolean("fencedLocation" , false);
    }

    public boolean getGelati() {
        return sharedPreferences.getBoolean("gelati" , false);
    }

    public boolean getIncidentGps() {
        return sharedPreferences.getBoolean("incidentGps" , false);
    }

    public boolean getLoneWorker() {
        return sharedPreferences.getBoolean("loneWorker" , false);
    }

    public boolean getManDown() {
        return sharedPreferences.getBoolean("manDown" , false);
    }

    public boolean getPtt() {
        return sharedPreferences.getBoolean("ptt" , false);
    }

    public boolean getMyDetails() {
        return sharedPreferences.getBoolean("myDetails" , false);
    }

    public boolean getNfcScan() {
        return sharedPreferences.getBoolean("nfcScan" , false);
    }

    public boolean getPasswordlessLogin() {
        return sharedPreferences.getBoolean("passwordlessLogin" , false);
    }

    public boolean getPushAlert() {
        return sharedPreferences.getBoolean("pushAlert" , false);
    }

    public boolean getSendPhotosWifi() {
        return sharedPreferences.getBoolean("sendPhotosWifi" , false);
    }

    public boolean getShowSimSetup() {
        return sharedPreferences.getBoolean("showSimSetup" , false);
    }

    public boolean getShowPhoneDialer() {
        return sharedPreferences.getBoolean("showPhoneDialer", false);
    }

    public boolean getShowMessages() {
        return sharedPreferences.getBoolean("showMessages", false);
    }

    // -------------
    // Int Settings

    public int getRssiCompensation() {
        return sharedPreferences.getInt("rssiCompensation", 0);
    }

    public boolean getLoneWorkerStandalone() {
        return sharedPreferences.getBoolean("loneWorkerStandalone", false);
    }

    public boolean getMultiplePictures() {
        return sharedPreferences.getBoolean("MultiplePictures", false);
    }


}