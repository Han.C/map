package com.daelibs.MAP;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.provider.Settings;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.daelibs.MAP.Data.DataRepository;
import com.daelibs.MAP.Data.DeviceInfo;

import java.io.File;
import java.text.SimpleDateFormat;


public class MainActivity extends AppCompatActivity {
    public static final String RECEIVER_INTENT = "RECEIVER_INTENT";
    public static final String RECEIVER_MESSAGE = "RECEIVER_MESSAGE";
    public String lastcontact;
    BroadcastReceiver mBroadcastReceiver;
    Intent serviceintent;
    Bundle lc;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);




        ActivityCompat.requestPermissions(MainActivity.this,
                new String[]{Manifest.permission.READ_PHONE_STATE},
                1);


        ActivityCompat.requestPermissions(MainActivity.this,
                new String[]{Manifest.permission.INTERNET},
                1);

        ActivityCompat.requestPermissions(MainActivity.this,
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                1);
        serviceintent = new Intent(this, WebService.class);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {


            if(!getPackageManager().canRequestPackageInstalls()) {
                startActivity(new Intent(android.provider.Settings.ACTION_MANAGE_UNKNOWN_APP_SOURCES, Uri.parse("package:com.daelibs.MAP")));
            }
            startForegroundService(serviceintent);
        } else {
            startService(serviceintent);
        }



        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Intent intent = new Intent();
            String packageName = getPackageName();
            PowerManager pm2 = (PowerManager) getSystemService(POWER_SERVICE);
            if (!pm2.isIgnoringBatteryOptimizations(packageName)) {
                intent.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
                intent.setData(Uri.parse("package:" + packageName));
                startActivity(intent);
            }
        }












        try
        {
            this.getSupportActionBar().hide();
        }
        catch (NullPointerException e){}




        if (savedInstanceState != null){
            lastcontact = savedInstanceState.getString("lastcontact");
        }

        setContentView(R.layout.mainactivity);
        TextView last_updated = (TextView) findViewById(R.id.last_updated);
        DataRepository repo = new DataRepository(getApplicationContext());
        if(repo.getlastcontact()!=null){
            lastcontact = "Last Contact:\n"+ repo.getlastcontact();
        }else{
            lastcontact = null;
        }


        PackageManager pm = this.getPackageManager();
        ApplicationInfo appInfo = null;
        try {
            appInfo = pm.getApplicationInfo("com.daelibs.MAP", 0);
            String appFile = appInfo.sourceDir;
            long installed = new File(appFile).lastModified();
            String itemDateStr = new SimpleDateFormat("dd-MM-yyyy HH:mm").format(installed);
            TextView last_installed = (TextView) findViewById(R.id.last_installed);
            last_installed.setText(itemDateStr);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }


        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(getApplicationContext(), "MAP Permission not granted", Toast.LENGTH_LONG).show();
        }else{
            DeviceInfo deviceInfo = Util.createDeviceConfig(MainActivity.this);
            TextView version = (TextView) findViewById(R.id.version);
            version.setText(deviceInfo.getApplicationVersionName());
        }



        if(lastcontact!=null){
            last_updated.setText(lastcontact);
        }

        Window window = this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(this,R.color.colorPrimary));


//        String loggerserial = Util.getSerial(this);
//
////        if(loggerserial!=null){
////            DeviceInfo deviceInfo = Util.createDeviceConfig(this);
////            String fdnumber = "F"+loggerserial.substring(1);
////            TextView deviceserial = (TextView) findViewById(R.id.SerialNumber);
////            deviceserial.setText(fdnumber);
////
////            TextView version = (TextView) findViewById(R.id.version);
////            version.setText(deviceInfo.getApplicationVersionName());
////            PackageManager pm = this.getPackageManager();
////            ApplicationInfo appInfo = null;
//////            try {
//////                appInfo = pm.getApplicationInfo("com.daelibs.MAP", 0);
//////                String appFile = appInfo.sourceDir;
//////                long installed = new File(appFile).lastModified();
//////                String itemDateStr = new SimpleDateFormat("dd-MM-yyyy HH:mm").format(installed);
//////                TextView last_installed = (TextView) findViewById(R.id.last_installed);
//////                last_installed.setText(itemDateStr);
//////
//////            } catch (PackageManager.NameNotFoundException e) {
//////                e.printStackTrace();
//////            }
////
////        }

        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String message = intent.getStringExtra(RECEIVER_MESSAGE);

//                String loggerserial = Util.getSerial(MainActivity.this);
                if(message!=null){
                    TextView last_updated = (TextView) findViewById(R.id.last_updated);
//                    Date currentTime = Calendar.getInstance().getTime();
                    String fdnumber = "F"+message.substring(1);
                    TextView deviceserial = (TextView) findViewById(R.id.SerialNumber);
                    deviceserial.setText(fdnumber);

                    DataRepository repo = new DataRepository(getApplicationContext());
                    if(repo.getlastcontact()!=null){
                        lastcontact = "Last Contact:\n"+ repo.getlastcontact();
                    }else{
                        lastcontact = null;
                    }

                    if(lastcontact!=null){
                        last_updated.setText(lastcontact);
                    }


//                    TextView last_updated = (TextView) findViewById(R.id.last_updated);
//
//                    last_updated.setText("Last Contact:\n"+currentTime.toString());


//                    Calendar calander = Calendar.getInstance();
//                    int cDay = calander.get(Calendar.DAY_OF_MONTH);
//                    int cMonth = calander.get(Calendar.MONTH) + 1;
//                    int cYear = calander.get(Calendar.YEAR);
//                    String selectedMonth = "" + cMonth;
//                    String selectedYear = "" + cYear;
//                    String sday = "" + cDay;
//                    String cHour = String.valueOf(calander.get(Calendar.HOUR_OF_DAY));
//                    String cMinute = String.valueOf(calander.get(Calendar.MINUTE));
//                    String cSecond = String.valueOf(calander.get(Calendar.SECOND));
//
//                    if(cHour.length()==1){
//                        cHour = "0"+cHour;
//                    }
//                    if(cMinute.length()==1){
//                        cMinute = "0"+cMinute;
//                    }
//                    if(cSecond.length()==1){
//                        cSecond = "0"+cSecond;
//                    }

//                    String lastupdatestring = sday + "/" + selectedMonth + "/" + selectedYear + "  "+ cHour + ":" + cMinute + ":"+ cSecond;
//                    last_updated.setText("Last Contact:\n"+lastupdatestring);
//                    lastcontact = "Last Contact:\n"+lastupdatestring;


                }

            }
        };







        Toast toast= Toast. makeText(getApplicationContext(),"MAP started",Toast. LENGTH_LONG);
        toast.show();
//        finish();
    }
    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mBroadcastReceiver);
        super.onDestroy();
    }


    @Override
    protected void onResume() {
        LocalBroadcastManager.getInstance(this).registerReceiver((mBroadcastReceiver),
                new IntentFilter(RECEIVER_INTENT)
        );
        super.onResume();
    }


//    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            // Get extra data included in the Intent
//            String message = intent.getStringExtra("Status");
//            Bundle b = intent.getBundleExtra("Location");
//
//            // Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
//        }
//    };


    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putString("lastcontact", lastcontact);
    }





}
