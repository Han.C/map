package com.daelibs.MAP;

import android.util.Log;

import com.daelibs.MAP.Data.ConfigResult;
import com.daelibs.MAP.Data.DeviceInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Iterator;

public class RegistrationClient extends AbstractSnfRpcClient {
    private static final String LOGTAG = "WebRegistrationService";
    public static class GetSerialResult {
        private String serialNumber;
        private String serviceInstanceName;
        private String resulttype;
        private String macsha;
        private String command;
        private String updateurl;
        private JSONArray apps;
        public String getSerialNumber() {
            return serialNumber;
        }
        public void setSerialNumber(String serialNumber) {
            this.serialNumber = serialNumber;
        }
        public String getServiceInstanceName() {
            return serviceInstanceName;
        }
        public void setServiceInstanceName(String serviceInstanceName) {
            this.serviceInstanceName = serviceInstanceName;
        }
        public void setupdateurl(String updateurl){this.updateurl = updateurl;}
        public String getupdateurl(){return updateurl;}
        public void setMacsha(String macsha){this.macsha = macsha;}
        public String getMacsha(){return macsha;}
        public void setresulttype(String resulttype){this.resulttype = resulttype;}
        public String getresulttype(){return resulttype;}
        public void setCommand(String command){this.command = command;}
        public String getCommand(){return command;}
        public void setApps(JSONArray apps){this.apps = apps;}

        public JSONArray getApps(){return apps;}
    }
    public static class PttConfig {
        private String serverAddress;
        private int crcInitialValue;

        public String getServerAddress() {
            return serverAddress;
        }

        public void setServerAddress(String serverAddress) {
            this.serverAddress = serverAddress;
        }

        public int getCrcInitialValue() {
            return crcInitialValue;
        }

        public void setCrcInitialValue(int crcInitialValue) {
            this.crcInitialValue = crcInitialValue;
        }
    }




    public RegistrationClient(String serviceAddress) {
        super("WebRegistrationService", serviceAddress);
        setEncryptBody(true);
    }



    public ConfigResult getConfig(String loggerSerial, DeviceInfo deviceInfo) {
        try {
            JSONObject params = new JSONObject();
            params.put("serialNumber", loggerSerial);
            params.put("deviceInfo", deviceInfoToJson(deviceInfo));

            JSONObject o = doRequest("getConfig", params);

            JSONObject settings = o.getJSONObject("settings");

            ConfigResult r = new ConfigResult();
            if (o.isNull("dataUploadUrl")) {
                r.setDataUploadUrl(null);
            } else {
                r.setDataUploadUrl(o.optString("dataUploadUrl"));
            }

            if (o.isNull("gelatiUploadUrl")) {
                r.setGelatiUploadUrl(null);
            } else {
                r.setGelatiUploadUrl(o.optString("gelatiUploadUrl"));
            }

            if (o.isNull("infoUrl")) {
                r.setInfoUrl(null);
            } else {
                r.setInfoUrl(o.optString("infoUrl"));
            }
            r.setServiceInstanceName(o.getString("serviceInstanceName"));
            r.setFilterInterval(settings.getInt("filterInterval"));
            JSONObject jFeatures = o.getJSONObject("features");
            ConfigResult.Feature[] features = new ConfigResult.Feature[jFeatures.length()];

            int i = 0;
            Iterator<String> jFeaturesKeys = jFeatures.keys();
            while (jFeaturesKeys.hasNext()) {
                String k = jFeaturesKeys.next();
                ConfigResult.Feature f = new ConfigResult.Feature();
                f.name = k;
                f.enabled = jFeatures.getBoolean(k);
                features[i] = f;
                i++;
            }

            r.setFeatures(features);

            return r;
        } catch (Exception e) {
            //TODO: log it permanently
            Log.i(LOGTAG, "getConfig exception", e);
            return null;
        }
    }

    public JSONObject getSettings(String loggerSerial) throws JSONException, IOException, SnfRpcException {
        JSONObject params = new JSONObject();
        params.put("serialNumber", loggerSerial);
        JSONObject result = doRequest("getSettings", params);
        return result.getJSONObject("settings");
    }

    private JSONObject deviceInfoToJson(DeviceInfo info) throws JSONException {
        JSONObject json = new JSONObject();
        json.put("imeiHash", info.getImeiHash());
        json.put("wifiMacHash", info.getWifiHash());
        json.put("simSerialEnc", info.getSimSerialEnc());
        json.put("simSerial", info.getSimSerial());
        json.put("imeiEnc", info.getImeiEnc());
        json.put("androidIdHash", info.getAndroidIdHash());
        json.put("applicationPackageName", info.getApplicationPackageName());
        json.put("applicationVersionName", info.getApplicationVersionName());
        json.put("applicationVersionCode", info.getApplicationVersionCode());

        json.put("androidManufacturer", info.getAndroidManufacturer());
        json.put("androidModel", info.getAndroidModel());
        json.put("androidVersion", info.getAndroidVersion());
        json.put("androidBuild", info.getAndroidBuild());

        json.put("androidBuildInfo", Util.getBuildInfoAsJson());

        return json;
    }

    public GetSerialResult activate(DeviceInfo info, String hintSite, String hintConfiguration)
            throws JSONException, IOException, SnfRpcException {

        JSONObject params = deviceInfoToJson(info);

//        params.put("activationCode", activationCode);

        if (hintSite != null) {
            params.put("hintSite", hintSite);
        }

        if (hintConfiguration != null) {
            params.put("hintConfiguration", hintConfiguration);
        }

        JSONObject o = doRequest("activate_daelibs_configurator", params);
        //JSONObject o = doRequest("getSerial", params); //TODO: switch to activate once central is ready
        GetSerialResult r = new GetSerialResult();

        r.setresulttype(o.getString("resulttype"));
        if(o.getString("resulttype").equals("2")){
            r.setSerialNumber(o.getString("serialNumber"));
            r.setupdateurl(o.getString("updateversion"));
            r.setMacsha(o.getString("macsha"));
        }else if(o.getString("resulttype").equals("3")){
            JSONArray apps = o.getJSONArray("apps");
            r.setApps(apps);
            r.setSerialNumber(o.getString("serialNumber"));
//            JSONArray apps = o.getJSONArray("apps");
////            r.setApps(apps);
//
//            r.setApps(o.getString("apps"));

        }else if(o.getString("resulttype").equals("66")){
            String command = o.getString("command");
            r.setCommand(command);
            r.setSerialNumber(o.getString("serialNumber"));

        }else{
            r.setSerialNumber(o.getString("serialNumber"));
            r.setServiceInstanceName(o.getString("serviceInstanceName"));
        }
        return r;

    }






    public GetSerialResult sendappvers(DeviceInfo info, String version, String appname)
            throws JSONException, IOException, SnfRpcException {

        JSONObject params = deviceInfoToJson(info);

        params.put("appName", appname);

        params.put("appVersion", version);

        JSONObject o = doRequest("app_version", params);

        return null;

    }















    public PttConfig getPttConfig(String loggerSerial)
            throws JSONException, IOException, SnfRpcException {

        JSONObject params = new JSONObject();
        params.put("serialNumber", loggerSerial);
        JSONObject result = doRequest("getPttConfig", params);

        if (result.isNull("crc_iv") || result.isNull("endpoint")) {
            return null;
        } else {
            PttConfig pttConfig = new PttConfig();
            pttConfig.setServerAddress(result.getString("endpoint"));
            pttConfig.setCrcInitialValue(result.getInt("crc_iv"));
            return pttConfig;
        }

    }

}
