package com.daelibs.MAP.UpdateService;


import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by ryan on 27/08/14.
 */
public class UpdateClient {

    public interface ProgressListener {
        public void onProgressUpdate(int percent);
    }

    private static final String TAG = "UpdateClient";

    public static class UpdateResult {
        public String version;
        public String macSha256;
        public String downloadUrl;
    }

    public static File downloadFile(Context context, String downloadUrl, ProgressListener progressListener) throws UpdateClientException {

        File resultTempFile = null;

        HttpURLConnection connection = null;
        BufferedOutputStream outputStream = null;
        InputStream inputStream = null;

        try {
            URL url = new URL(downloadUrl);
            connection = (HttpURLConnection) url.openConnection();
            connection.setReadTimeout(300000);
            File externalDir = context.getExternalFilesDir(null);
            File updateDir = new File(externalDir, "update");

            if (!updateDir.isDirectory()) {
                if (!updateDir.mkdir()) {
                    throw new UpdateClientException("downloadFile: cannot make updates dir");
                }
            }
            for(File tempFile : updateDir.listFiles()) {
                tempFile.delete();
            }
            File tmp = File.createTempFile("update-", ".tmp", updateDir);

            if (progressListener != null) {
                progressListener.onProgressUpdate(0);
            }

            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                outputStream = new BufferedOutputStream(new FileOutputStream(tmp));

                inputStream = connection.getInputStream();

                long length = connection.getContentLength();
                long received = 0;

                byte[] recvbuf = new byte[2048];

                while (received < length) {
                    int count = inputStream.read(recvbuf);
                    outputStream.write(recvbuf, 0, count);
                    if (progressListener != null) {
                        progressListener.onProgressUpdate((int) (((float) received / (float) length) * 100));
                    }
                    received += count;
                }

                if (progressListener != null) {
                    progressListener.onProgressUpdate(100);
                }
                outputStream.flush();
                resultTempFile = tmp; // Download Success!
            } else {
                throw new UpdateClientException(String.format("Package Download invalid response %d", connection.getResponseCode()));
            }
        } catch (IOException e) {
            String cause = "unknown";
            if (e.getCause() != null) {
                cause = e.getCause().getMessage();
            }
            throw new UpdateClientException("Error downloading package: " + cause , e);

        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    Log.i(TAG, "inputStream.close exception", e);
                }
            }

            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    Log.i(TAG, "outputStream.close exception", e);
                }
            }

            if (connection != null) {
                connection.disconnect();
            }
        }
        return resultTempFile;
    }









    public static UpdateResult checkForUpdate(String updateUrl) throws UpdateClientException {

        HttpURLConnection connection = null;
        InputStream inputStream = null;
        UpdateResult updateResult = null;

        try {
            URL url = new URL(updateUrl);

            connection = (HttpURLConnection) url.openConnection();

            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                inputStream = connection.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                StringBuffer sb = new StringBuffer();
                String line;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                JSONObject resultJson = new JSONObject(sb.toString());
                updateResult = new UpdateResult();

                updateResult.macSha256 = resultJson.getString("macSha256");
                updateResult.version = resultJson.getString("version");
                updateResult.downloadUrl = resultJson.getString("downloadUrl");




            } else {
                throw new UpdateClientException(String.format("Invalid Response Code: %d", connection.getResponseCode()));
            }
        } catch (java.io.IOException e) {
            Log.e(TAG, "Network Error", e);
            throw new UpdateClientException("Network Error: " + e.getCause().getMessage(), e);
        } catch (JSONException e) {
            Log.e(TAG, "checkForUpdate exception", e);
            throw new UpdateClientException("Protocol Error", e);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    Log.i(TAG, "inputStream.close exception", e);
                }
            }
            if (connection != null) {
                connection.disconnect();
            }
        }
        return updateResult;
    }







    public static void runCommand(Context applicationContext, List<String>  commands) {

        try {


            List<String> cmds = new ArrayList<String>();
            cmds.add("su");

            for (String s : commands) {
                cmds.add(s);
            }

            ProcessUtils.runProcessNoException(cmds.toArray(new String[cmds.size()]));






        } catch (Exception e) {
            Log.e("SnfRpcClient Error",e.toString());
            try {

                List<String> cmds = new ArrayList<String>();
                cmds.add("wisu");
                for (String s : commands) {
                    cmds.add(s);

                }
                Log.e("SnfRpcClient Error",cmds.toString());
                ProcessUtils.runProcessNoException(cmds.toArray(new String[cmds.size()]));

            }catch(Exception e2) {
                Log.e("COMMAND ERROR",e2.toString());

            }
        }

        try {
            Thread.sleep(500);

        } catch (InterruptedException e) {


        }


//        daelockHelper.unlockWithoutCode();


//        Intent intent = new Intent(Intent.ACTION_VIEW);
//        intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        applicationContext.startActivity(intent);

    }





    public static void installApk(Context applicationContext, File file) {

        try {
//            Runtime.getRuntime().exec("chmod 777 " + file.getAbsolutePath());
            ProcessUtils.runProcessNoException("su", "0", "pm", "install", "-r", file.getAbsolutePath());
        } catch (Exception e) {
            try {
                ProcessUtils.runProcessNoException("wisu", "0", "pm", "install", "-r", file.getAbsolutePath());
            }catch(Exception e2) {
                Log.e("INSTALL COMMAND ERROR",e2.toString());

            }
        }

        try {
            Thread.sleep(500);

        } catch (InterruptedException e) {


        }


    }




    public static void doUpdate(Context context, String updateUrl, ProgressListener progressListener, String updatemacSha256) throws UpdateClientException {
                File file = downloadFile(context, updateUrl, progressListener);

                if (file != null) {
                    String mac = null;
                    try {
                        mac = generateHmac(file);
                    } catch (Exception e) {
                        Log.i(TAG, "doUpdate error generating hmac");
                        throw new UpdateClientException("Error validating download", e);
                    }
                    if (mac != null && mac.equals(updatemacSha256)) {
                        installApk(context, file);
                    } else {
                        throw new UpdateClientException("Download verification failed");
                    }
                }

    }











    public static String generateHmac(File file) throws IOException, UpdateClientException {
        final String K = "Bai2xu2so9Teebu!uaGush3J";
        final String MAC_ALGO = "HmacSHA256";
        final int BUF_SIZE = 2048;

        FileInputStream inputStream = new FileInputStream(file);
        try {
            SecretKeySpec key = new SecretKeySpec((K).getBytes("UTF-8"), MAC_ALGO);
            Mac mac = Mac.getInstance(MAC_ALGO);
            mac.init(key);

            byte[] buffer = new byte[BUF_SIZE];

            while (true) {
                int count = inputStream.read(buffer, 0, BUF_SIZE);
                if (count == -1) {
                    break;
                }
                mac.update(buffer, 0, count);
            }

            byte[] bytes = mac.doFinal();

            StringBuffer hash = new StringBuffer();
            for (int i = 0; i < bytes.length; i++) {
                String hex = Integer.toHexString(0xFF & bytes[i]);
                if (hex.length() == 1) {
                    hash.append('0');
                }
                hash.append(hex);
            }
            return hash.toString();
        } catch (NoSuchAlgorithmException e) {
            Log.e(TAG, "generateHmac", e);
            throw new UpdateClientException("Error validating file", e);
        } catch (InvalidKeyException e) {
            Log.e(TAG, "generateHmac", e);
            throw new UpdateClientException("Error validating file", e);
        } finally {
            inputStream.close();
        }
    }

    public static PackageInfo getPackageInfo(Context context) throws PackageManager.NameNotFoundException {
        String packageName = context.getPackageName();
        return context.getPackageManager().getPackageInfo(packageName, 0);
    }

    public static void cleanupUpdateFiles(Context context) {
        //oldDir was the original place where the updates were saved
        File oldDir = new File(Environment.getExternalStorageDirectory(), "SeeknFind");

        if (oldDir.isDirectory()) {
            for (File f : oldDir.listFiles()) {
                if (f.getName().startsWith("download-")) {
                    f.delete();
                }
            }
            oldDir.delete();
        }


        File externalDir = context.getExternalFilesDir(null);
        File updateDir = new File(externalDir, "update");

        if (updateDir.isDirectory()) {
            for (File f : updateDir.listFiles()) {
                f.delete();
            }
        }
    }
}





class ProcessUtils {
    Process process;
    int errCode;
    public ProcessUtils(String ...command) throws IOException, InterruptedException{
        ProcessBuilder pb = new ProcessBuilder(command);
        this.process = pb.start();
        this.errCode = this.process.waitFor();
    }

    public int getErrCode() {
        return errCode;
    }

    public String getOutput() throws IOException {
        InputStream inputStream = process.getInputStream();
        InputStream errStream = process.getErrorStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
        String line;
        StringBuilder sb = new StringBuilder();
        while ((line = br.readLine()) != null) {
            sb.append(line + System.getProperty("line.separator"));
        }

        br = new BufferedReader(new InputStreamReader(errStream));
        while ((line = br.readLine()) != null) {
            sb.append(line + System.getProperty("line.separator"));
        }
        return sb.toString();
    }


    public static String runProcess(String ...command) throws IOException, InterruptedException {
        ProcessUtils p = new ProcessUtils(command);
        if (p.getErrCode() != 0) {
            // err
            Log.e("AppStore", "ERROR1");
        }
        return p.getOutput();
    }

    public static void runProcessNoException(String ...command ) throws IOException, InterruptedException {

    runProcess(command);

    }
}




