package com.daelibs.MAP.UpdateService;


public class UpdateClientException extends Exception {
    public UpdateClientException() {
        super();
    }

    public UpdateClientException(String message) {
        super(message);
    }

    public UpdateClientException(String message, Throwable cause) {
        super(message, cause);
    }
}
