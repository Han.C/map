import hashlib
import hmac
import json
import os
import re
import sys


key = "Bai2xu2so9Teebu!uaGush3J"

def gen_hmac(fname):
    mac = hmac.new(key.encode("UTF-8"), None, hashlib.sha256)

    with open(fname, "rb") as file:
        mac.update(file.read())

    return mac.hexdigest()

def gen_update_file(fname):
    mac = gen_hmac(fname)

    bfname = os.path.basename(fname)

    if bfname.startswith("MAP-") and bfname.endswith(".apk"):
        flavor, sep, versionname = bfname[3:-4].partition("-")
        div = versionname.split("-")
        if len(div)==4 :
            ver = div[0]
            config = div[1] + "-" + div[2]
            build = div[3]
        elif len(div) == 3 :
            ver = div[0]
            config = div[1]
            build = div[2]

    values = {
        "downloadUrl": "http://u.seeknfind.com.au/android/com.daelibs.MAP/{}/{}".format(config, bfname),
        "version": versionname,
        "macSha256": mac
    }

    return json.dumps(values, indent=4)

if __name__ == "__main__":
    try:
        fname = sys.argv[1]
    except IndexError:
        print("Usage: {} filename".format(sys.args[0]))
        sys.exit(1)

    print(gen_update_file(fname))
    




