import os
import subprocess

def parse_version(s):
    fields = s.split(".")
    if len(fields) != 3:
        raise ValueError
    return tuple(int(f) for f in fields)

def locate_newest_build_tools(location):
    build_tools_path = os.path.join(location, "build-tools")

    all_sub_dirs = list(os.listdir(build_tools_path))

    candidates = []

    for s in all_sub_dirs:
        sub = os.path.join(build_tools_path, s)
        if os.path.isdir(sub):
            try:
                entry = (s, parse_version(s))
                candidates.append(entry)
            except ValueError:
                pass

    candidates.sort(key=lambda x : x[1])
    
    return os.path.join(build_tools_path, candidates[-1][0])

def extract_version(apk_file):
    build_tools_path = locate_newest_build_tools(locate_sdk())

    aapt = os.path.join(build_tools_path, 'aapt.exe')

    output = subprocess.check_output([aapt, 'd', 'xmltree', apk_file, 'AndroidManifest.xml'])
    output = str(output, "utf-8") #not sure what the correct charset is

    lines = output.splitlines()

    for l in lines:
        l = l.strip()
        if l.startswith("A: android:versionName"):
            s = l.find("\"")
            e = l.find("\"", s+1)
            return l[s+1:e]

def _unesc_prop_val(val):
    # poor mans version
    s = ""
    escape = False
    for c in val:
        if escape:
            s += c
            escape = False
        else:
            if c == "\\":
                escape = True
            else:
                s += c
    return s
        
def locate_sdk():
    with open("local.properties") as f:
        for line in f:
            if line.startswith("sdk.dir"):
                key, sep, val = line.partition("=")
                return _unesc_prop_val(val.strip())
    return None

if __name__ == "__main__":
    print(extract_version(os.path.join("app", "app-release.apk")))
